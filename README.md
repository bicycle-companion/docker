# BicycleCompanion build environment

This dockerfile can be used to build a docker image with everything needed to build and flash
the BicycleCompanion firmware. This image is used also for CI builds at GitLab so it is ensured 
to build correctly here.

The corresponding docker image is already built and uploaded to dockerhub as v01d/bicycle-companion, so
you don't need to built it yourself

## Building BicycleCompanion firmware on your computer

First, get the code:

  git clone --recursive http://gitlab.com/bicycle-companion/firmware

Then, start the container like this:

  docker run --privileged --rm -it -v /dev/bus/usb:/dev/bus/usb -v <full path to firmware directory>:/root/firmware

Now, inside the container, simply build the firmware:

  cd firmware
  make configure
  make mconf
  make

You can then flash it using the following command:

  teensy_loader_cli --mcu=mk20dx256 -wv NuttX/nuttx/nuttx.hex

For rebuilding (in case you change the code) you only need to use:

  make

and flash again.

