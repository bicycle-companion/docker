FROM ubuntu:xenial

RUN apt-get update && apt-get install -y \
	vim \
	build-essential \
	git \
	gcc-arm-none-eabi \
	libusb-dev \
	gperf \
	flex \
	bison \
	libncurses-dev

RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN git clone https://github.com/PaulStoffregen/teensy_loader_cli && \
	cd teensy_loader_cli && \
	make && \
	cp teensy_loader_cli /usr/bin && \
	cd .. && \
	rm -rf teensy_loader_cli

WORKDIR /root
